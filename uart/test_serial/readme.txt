changelog:

2016-01-22
不传参数运行uart_raw，默认打开/dev/tySP0；
将串口0的TXD0和RXD0通过杜邦线短接在一起，运行可执行文件uart_raw，即可看到终端打印出接收到的数据。