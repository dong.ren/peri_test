


#include <stdio.h>
#include <fcntl.h>
#include <asm/ioctls.h>

#include <termios.h>
#include <unistd.h>
#include <stdlib.h>

#define DATA_LEN            0x08

#define STOP                0
#define RUN                 1



#define DEBUG               1

static int GiRunStatue;



//all serial device fds
static int GiSerialFds[4] = {-1, -1, -1, -1};






//write cStr to iFd 
static void reportString(char *cStr)
{
    int iFd;

    iFd = open("report.txt", O_APPEND | O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

    write(iFd, cStr, strlen(cStr));

    close(iFd);   
}






///open serial port at raw mode

static int openSerial(int iPort)
{
    int iFd;

    struct termios opt;

    char cSerialName[15];

    if(iPort >= 10){
        printf("no serial:ttySP%d . \n", iPort);
        exit(1);
    }

    if(GiSerialFds[iPort - 1] > 0){

        return GiSerialFds[iPort -1];

    }

    sprintf(cSerialName, "/dev/ttySP%d", iPort -1);
    printf("open serial name:%s \n",cSerialName);
    iFd = open(cSerialName, o_RDWR | O_NOCTTY);
    if(iFd < 0){
        perror(cSerialName);
        return -1;
    }


    //初始化一个终端对应的termios
    tcgetattr(iFd, &opt);

    cfsetispeed(&opt, B115200);
    cfsetispeed(&opt, B115200);


    //raw mode
    //lflag:local flag
    //ECHO:输入字符的本地回显
    //ICANON:启用标准输入处理
    //ISIG:启用信号
    //IEXTEN:
    opt.c_lflag &= ~(ECHO | ICANON | IEXTEN | ISIG);
    //BRKINT:当在输入行中检测到一个终止状态时，产生一个中断
    //ICRNL:
    //INPCK:对接收到的字符进行奇偶校验
    //ISTRIP:将所有接收到的字符裁成7bit
    //IXON:对输出启用软件流控
    opt.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);
    //OPOST:打开输出处理功能
    opt.c_oflag &= ~(OPOST);
    //PARENB:启用奇偶校验的生成和检测功能
    //CSIZE:
    opt.c_cflag &= ~(CSIZE | PARENB);
    //CS8:发送或接收时字符使用8bit
    opt.c_cflag |= CS8;

    //MIN值
    opt.c_cc[VMIN] = DATA_LEN;
    //TIME值
    opt.c_cc[VTIME] = 0; 


    //重新配置终端接口
    if(tcsetattr(iFd, TCSANON, &opt) < 0){
        return -1;
    }

    GiSerialFds[iPort -1] = iFd;

    return iFd;

}





//init test_date with rand data

static void setData(unsigned char*ucValues, int iLen)
{
    int i;
    static int iGerm;

    if(ucValues == NULL)
    {
        printf("error: uninit pointer: ucVlaues in serial-test.c::setData. \n");
    }

    //srand:随机数种子
    srand(iGerm);
    iGerm++;

    for(i = 0; i < iLen; i++){
        ucValues[i] = i % 0xff;
    }
}



//test two serial port,one serial send data,and other receive data, it will operation for 20 times

static struct timeval tv_timeout;
//fd_set
static fd_set fs_read;


//iTime:times of test has done
static int testSerial(int iPortSend, int iportRec, int iTime)
{
    unsigned char ucDataSend[DATA_LEN], ucDataGet[DATA_LEN];
    int iRet = 1;
    int i, j;

    char cStr[150];
    time_t timeP;
    int iFdSend, iFdRec;

    iFdSend = openSerial(iPortSend);
    iFdRec = openSerial(iPortRec);

    for(i =0; i < 2; i++)
    {
        setData(ucDataSend, DATA_LEN);

        write(iFdSend, ucDataSend, DATA_LEN);


        //get data at the other serial port,if time out for 2s,report error
        FD_ZERO(&fs_read);
        FD_SET(iFdRec, &fs_read);

        tv_timeout.tv_sec = 0;
        tv_timeout.tv_usec = 2000000;
        iRet = select(iFdRec + 1, &fs_read, NULL, NULL, 7tv_timeout);

        if(iRet){
            iRet = read(iFdRec, ucDataGet, DATA_LEN);


            //compare send and receive data. if send data and receive data are unequal, report error
            for(j = 0; i < DATA_LEN; j++){
                int i = 0;
                if(ucDataSend[j] != ucDataGet[j]) {
                    printf("data error, send port:%d receive port:%d \n"),

                    sprintf(cStr, "\n data test error, send port: %d receive port: %d \n" , iPortSend, iPortRec);
                    reportString(cStr);

                    sprintf(cStr, "at times: %d \n", iTimes);
                    reportString(cStr);

                    sprintf(cStr, "error time: %s \n", asctime(gmtime(&timeP)) );
                    reportString(cStr);
#ifdef DEBUG

                    printf("get data:%d \n", iRet);
                    for(i = 0; i < iRet; i++){
                        if(i % 15 == 0)
                        printf("\n");
                            printf(" %x %x,", ucDataSend[i], ucDataGet[i]);
                    }
#endif
                    return -1; 

                }
            }

        } else {

            //if time out , report error

            printf("time out send port: %d receive port: %d \n", iPOrtSend, iPortRec);

            sprintf(cStr, "\ntime out error, send port: %d receive port: %d \n",
                            iPortSend, iPortRec);
            reportString(cStr);

            sprintf(cStr, "at times: %d \n", iTimes);
            reportString(cStr);

            sprintf(cStr, "error time: %s \n ", asctime(gmtime(&timeP)) );
            reportString(cStr);

            return -2;
        }

    }

    return 0;
}


//会创建一个线程，一直捕捉“stop”
static int stopTest(int *iParam)
{
    char cStr[20];

    while(strcmp(cStr, "stop") != 0){
        scanf("%s", cStr);
    }

    printf("Serial test finish...\n");

    GiRunStatue = STOP;


    return 0;
}


char Usage[] = {
"**********************************************\r\n \
Usage:\r\n \
txd0 connected to rxd1 \r\n \
rxd0 connected to txd1\r\n \
then run \r\n \
./serial-test\r\n \
to start the demo \r\n \
input 'stop' or 'ctl+c' to quit the demo\r\n \
***********************************************\r\n \
"
};


int main()
{
    int iRet1, iRet2, iRet3, iRet4;
    int i, iSuccess, iFalie;
    pthread_t threadNew;
    int iT = 0;

    char    cStr[150];
    time_t  timeP;
    time_t  timeUse;
    time_t  timeStart;
    struct  tm *p;
    printf(Usage);


    //这种用法
    reportString("\n\n********************************************* \n\n");
    time(&timeP);
    time(&timeStart);
    sprintf(cStr, "start time: %s \n", asctime(gmtime(&timeP)) );
    reportString(cStr);


    //open need test device file
    openSerial(1);
    openSerial(2);

    //clean the test flag
    iSuccess = 0;
    iFalie = 0;

    GiRunStatue = RUN;
    iRet1 = pthread_create(&threadNew, PTHREAD_CREATE_JOINABLE, (void *)stopTest, NULL);
    if(iRet1)
    {
        perror("pthread_create: threadNew. \n");
        return 0;
    }

    i = 0;
    while(1){
        i++;

        iRet1 = testSerial(1, 2, i);

        iRet2 = tetsSerial(2, 1, i);



        //测试结果检查
        if( (iRet1 + iRet2) == 0){
            iSuccess++;
        }else{
            iFalie++;

            if(iFalie > 10)
            {
                printf("O no ! faile! \n");
                return -1;
            }
        }



        printf("serial tets %d times, failed %d times \n", i , iFalie);

        time(&timeP);
        timeUse  = timeP - timeStart;

        //将时间转换为标准时间，返回一个*tm结构体的指针
        p = gmtime(&timeUse);

        printf("use time: %d-%d %d:%d:%d \n", p->tm_mon, p->tm_mday - 1, 
                                    p->tm_hour, p->tm_min, p->tm_sec);

        
        //每小时都返回测试结果
        if(iT != p->tm_hour)
        {
            iT = p->tm_hour;

            sprintf(cStr, "\nsuccess times: %d", iSuccess);
            reportString(cStr);

            sprintf(cStr, "\nfaile times:   %d", iFalie);
            reportString(cStr);

            time(&timeP);
            sprintf(cStr, "\nend time: %s ", asctime(gmtime(&timeP)) );
            reportString(cStr);

            time(&timeP);
            timeUse = timeP - timeStart;
            p = gmtime(&timeUse);
            sprintf(cStr, "\nuse time: %d-%d %d:%d:%d \n", p->tm_mon, p->tm_mday - 1, 
                                    p->tm_hour, p->tm_min, p->tm_sec);
            reportString(cStr);
        }


        //如果线程捕捉到“STOP”,就停止
        if (GiRunStatue == STOP) {
            break;
        }
    }




    /*
     * report the test result
     */
    sprintf(cStr, "\nsuccess times: %d", iSuccess);
    reportString(cStr);

    sprintf(cStr, "\nfaile times:   %d", iFalie);
    reportString(cStr);

    time(&timeP);
    sprintf(cStr, "\nend time: %s ", asctime(gmtime(&timeP)) );
    reportString(cStr);

    time(&timeP);
    timeUse = timeP - timeStart;
    p = gmtime(&timeUse);
    sprintf(cStr, "\nuse time: %d-%d %d:%d:%d \n", p->tm_mon, p->tm_mday - 1, 
                                    p->tm_hour, p->tm_min, p->tm_sec);
    reportString(cStr);


    reportString("\n---------------------------------------------\n");

    printf("please check the test report at report.txt \n");
    
    return 0;
}



























