#include     <stdio.h>
#include     <stdlib.h> 
#include     <unistd.h>  
#include     <sys/types.h>
#include     <sys/stat.h>
#include     <fcntl.h> 
#include     <termios.h>
#include     <errno.h>  

#include "iic.h"

#define I2C_ADDR  0xA0
#define DATA_LEN 15

int main(void)
{
	unsigned int uiRet;
	int i;

	unsigned char tx_buf[DATA_LEN];
	unsigned char rx_buf[DATA_LEN];
	unsigned char addr[2] ;

	addr[0] = 0x00;
	    
	GiFd = open("/dev/i2c-1", O_RDWR);   
	if(GiFd == -1)
    	perror("open i2c-1\n");

	uiRet = ioctl(GiFd, I2C_SLAVE, I2C_ADDR >> 1);
	if (uiRet < 0) {
	printf("setenv address faile ret: %x \n", uiRet);
	return -1;
 	}

	for (i = 0; i < DATA_LEN; i++) //写将要发送的数据到 发送buf
    	tx_buf[i] = i+1; 
	
	for (i = 0; i < DATA_LEN; i++){ //写要发送的数据到 24c02
		addr[0] = 0x00+i; //当前地址
		write(GiFd, addr, 1);  //写地址
		write(GiFd, &tx_buf[i], 1); //写数据
	}
	
	addr[0] = 0x00; //当前地址
	write(GiFd, addr, 1);  //写地址
	read(GiFd, rx_buf, DATA_LEN); //读数据

	printf("read from eeprom:");
	for(i = 0; i < DATA_LEN; i++) {
     	printf(" %x", rx_buf[i]);
	}

	printf("\n");

	return 0;
}

